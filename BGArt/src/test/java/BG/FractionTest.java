package BG;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class FractionTest {


    @Test
    public void NullConstructor() {
        Fraction F = new Fraction();

        assertEquals(F.getNumerator(), 0);
        assertEquals(F.getDenominator(), 1);

    }

    @Test
    public void ConstructorNum() {
        Fraction F = new Fraction(5);
        assertEquals(F.getNumerator(), 5);
        assertEquals(F.getDenominator(), 1);
    }

    @Test
    public void ConstructorDouble() {
        Fraction F = new Fraction(5, 7);
        assertEquals("5/7", F.ConvToString());
    }

    @Test
    public void Constants(){
        Fraction F = new Fraction(5, 7);

        assertTrue(0.0== F.getZero().Calc());

        assertTrue(1.0== F.getOne().Calc());
    }


    @Test
    public void ConvString() {
        Fraction F = new Fraction(5, 7);
        assertEquals(F.getNumerator(), 5);
        assertEquals(F.getDenominator(), 7);
    }

    @Test
    public void CalcTest()
    {
        Fraction F = new Fraction(5,2);
                assertTrue(F.Calc()==2.5);
    }

    @Test
    public void TestEgal()
    {
        Fraction F = new Fraction(5,2);
        Fraction F2= new Fraction(10,4);
        assertTrue(F.isEqual(F2));

        Fraction F3= new Fraction(12,4);
        assertTrue(!F.isEqual(F3));
    }


}
